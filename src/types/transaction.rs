use serde::{Serialize,Deserialize};
use ring::signature::{ED25519, UnparsedPublicKey, Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use rand::Rng;
use crate::types::hash::{H256, Hashable};
use bincode::{serialize, deserialize};
use super::address::Address;

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Transaction {
    sender: Address,
    receiver: Address,
    value: f32,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct SignedTransaction {
    transaction : Transaction,
    sig : Vec<u8>,
}

impl SignedTransaction{
    fn fromTransact(t:Transaction, key: &Ed25519KeyPair) -> Self{
        let signature = sign(&t, &key);
        let signature_vector: Vec<u8> = signature.as_ref().to_vec();
        return SignedTransaction{
            transaction : t,
            sig : signature_vector,
        }
    }
}

impl Hashable for SignedTransaction{
    fn hash(&self) -> H256{
        let bytes = bincode::serialize(&self).unwrap();
        return ring::digest::digest(&ring::digest::SHA256, &bytes).into();
    }
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    // unimplemented!()
    let ser_t = serialize(&t).unwrap();
    key.sign(&ser_t)
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &[u8], signature: &[u8]) -> bool {
    // unimplemented!()
    let ser_t = serialize(&t).unwrap();
    // let public_key_bytes = public_key.as_ref();
    let peer_public_key =
    UnparsedPublicKey::new(&ED25519, public_key);
    let result = peer_public_key.verify(&ser_t, signature);
    result.is_ok()
}

#[cfg(any(test, test_utilities))]
pub fn generate_random_transaction() -> Transaction {
    let mut rng = rand::thread_rng();
    let price :f32 = rng.gen_range(1..1000) as f32;
    let ad1_u : u8 = rng.gen_range(1..250);
    let ad2_u : u8 = rng.gen_range(1..250);
    let ad1 = [ad1_u;20];
    let ad2 = [ad2_u;20];
    let Ad1 = Address::from(&ad1);
    let Ad2 = Address::from(&ad2);
    Transaction{
        sender:Ad1,
        receiver:Ad2,
        value:price,
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. BEFORE TEST

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::key_pair;
    use ring::signature::KeyPair;


    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, key.public_key().as_ref(), signature.as_ref()));
    }
    #[test]
    fn sign_verify_two() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        let key_2 = key_pair::random();
        let t_2 = generate_random_transaction();
        assert!(!verify(&t_2, key.public_key().as_ref(), signature.as_ref()));
        assert!(!verify(&t, key_2.public_key().as_ref(), signature.as_ref()));
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. AFTER TEST
