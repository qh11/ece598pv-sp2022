use super::hash::{Hashable, H256};
use ring::digest;
use std::collections::VecDeque;



/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    nodes: VecDeque<VecDeque<H256>>,
}
 

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        let curr_size = data.len();
        let mut nodes : VecDeque<VecDeque<H256>> = VecDeque::new();
        let mut deque : VecDeque<H256> = VecDeque::new();
        for datum in data {
            deque.push_back(datum.hash());
        }
        if curr_size % 2 != 0 {
            deque.push_back(data[curr_size - 1].hash());
        }
        nodes.push_back(deque);
        loop {
            if nodes[0].len() == 1 {
                return Self{ 
                    nodes: nodes,
                }
            }
            deque = VecDeque::new();
            let curr_len = nodes[0].len();
            for i in (1..curr_len).step_by(2) {
                let left_hash = nodes[0][i-1];
                let right_hash = nodes[0][i];
                let curr_hash : H256 = hash_from(&left_hash, &right_hash);
                deque.push_back(curr_hash);
            }
            if deque.len() != 1 && deque.len() % 2 == 1 {
                deque.push_back(deque[deque.len()-1]);
            }
            nodes.push_front(deque);
        }
    }

    pub fn root(&self) -> H256 {
        self.nodes[0][0]
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut result : Vec<H256> = Vec::new();
        let mut pos = index;
        for i in (1..self.nodes.len()).rev(){
            if pos % 2 == 1{
                result.push(self.nodes[i][pos-1]);
            } else {
                result.push(self.nodes[i][pos+1]);
            }
            pos /= 2;
        }
        result
    }
    
}

fn hash_from(left: &H256, right: &H256) -> H256{
    let mut combined = [0u8; 64];
    combined[..32].copy_from_slice(left.as_ref());
    combined[32..64].copy_from_slice(right.as_ref());
    digest::digest(&digest::SHA256, &combined).into()
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut curr_hash = datum.clone();
    let mut pos = index;
    for hash in proof{
        if pos % 2 == 1{
            curr_hash = hash_from(&hash, &curr_hash);
        } else {
            curr_hash = hash_from(&curr_hash, &hash);
        }
        pos /= 2;
    }
    curr_hash == *root

}
// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. BEFORE TEST

#[cfg(test)]
mod tests {
    use crate::types::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn merkle_root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn merkle_proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn merkle_verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. AFTER TEST
